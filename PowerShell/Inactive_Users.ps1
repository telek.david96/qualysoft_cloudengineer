#Define variables
$deactivateafterdays = 60
$SavePath = 'C:\Users\azvmad2019_rootadmin\Documents'

function Revoke-User{
    param(
        [string] $Username
    )
    #Disable the AD account based on the Username
    Disable-ADAccount -Identity $Username

    #Export the MemberOf of the user then revoke them
    Get-ADPrincipalGroupMembership $Username | Select-Object Name, GroupCategory, GroupScope | Export-Csv -Path "$SavePath\$($Username)_$(Get-Date -Format "yyyy_MM_dd_HH_mm").csv" -Encoding utf8 -NoTypeInformation
    (Get-ADUser -Identity $Username -Properties MemberOf).MemberOf | ForEach-Object {Remove-ADGroupMember -Identity $_ -Members $Username}

    #Add an expiration date to the account
    Set-ADAccountExpiration -Identity $Username -DateTime (Get-Date)

    #Add time in this format MMddyyyyHHmm to extensionAttribute1
    Set-ADUser -Identity $Username -Add @{extensionAttribute1= Get-Date -Format "MMddyyyyHHmm"}
}

#Get all AD users with SAMAccountname and LastLogonDate
Get-ADUser -Filter * -Properties SAMAccountname,LastLogonDate | ForEach-Object {
    $SAMAccountname = $_.SAMAccountname
    
    #Check if users last logon date is older than today minus specified days
    if($_.lastlogondate -lt (Get-date).AddDays(-$deactivateafterdays)){
        #Call revoke function with samaccountname
        Revoke-User -Username "$SAMAccountname"

        #Log who was revoked to terminal
        Write-Host "$SAMAccountname - $($_.lastlogondate) - Revoked"
    }
}