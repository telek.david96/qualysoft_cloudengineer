#Define variables
$deactivateafterdays = 60
$SavePath = 'C:\Users\azvmad2019_rootadmin\Documents'

function Get-RandomPassword {
    param (
        [Parameter(Mandatory)]
        [int] $length,
        [int] $uppercount = 1,
        [int] $lowercount = 1,
        [int] $specialcount = 1)

    #Check if the parametars is correct else throw an error about it
    if($uppercount + $lowercount + $specialcount -gt $length) {
        throw "Error: Length needs to be greater than the sum of criteria"
    }
    else{
        $numbercount = $length - $uppercount - $lowercount - $specialcount
    }

    #Define the character set that can be in the password
    $uCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    $lCharSet = "abcdefghijklmnopqrstuvwxyz"
    $nCharSet = "0123456789"
    $sCharSet = " !`"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
    
    #Define the arraylist so I can use the Add method
    $unsecpassword = [System.Collections.ArrayList]@()
    
    #Fill the array with randoms for the given amount
    for ($i = 0; $i -lt $uppercount; $i++){
        $null = $unsecpassword.Add($uCharSet[(Get-Random -Maximum $uCharSet.Length)])
    }
    for ($i = 0; $i -lt $lowercount; $i++){
        $null = $unsecpassword.Add($lCharSet[(Get-Random -Maximum $lCharSet.Length)])
    }
    for ($i = 0; $i -lt $specialcount; $i++){
        $null = $unsecpassword.Add($sCharSet[(Get-Random -Maximum $sCharSet.Length)])
    }
    for ($i = 0; $i -lt $numbercount; $i++){
        $null = $unsecpassword.Add($nCharSet[(Get-Random -Maximum $nCharSet.Length)])
    }

    #Shuffle the array and join them to one string. After that convert to secure string
    $secpassword = ConvertTo-SecureString -String (($unsecpassword | Sort-Object {Get-Random}) -join "") -AsPlainText -Force

    return $secpassword
}
function Revoke-User{
    param(
        [string] $Username,
        [SecureString] $NewPassword
    )
    #Disable the AD account based on the Username
    Disable-ADAccount -Identity $Username

    #Export the MemberOf of the user then revoke them
    Get-ADPrincipalGroupMembership $Username | Select-Object Name, GroupCategory, GroupScope | Export-Csv -Path "$SavePath\$($Username)_$(Get-Date -Format "yyyy_MM_dd_HH_mm").csv" -Encoding utf8 -NoTypeInformation
    (Get-ADUser -Identity $Username -Properties MemberOf).MemberOf | ForEach-Object {Remove-ADGroupMember -Identity $_ -Members $Username}

    #Add an expiration date to the account
    Set-ADAccountExpiration -Identity $Username -DateTime (Get-Date)

    #Add time in this format MMddyyyyHHmm to extensionAttribute1
    Set-ADUser -Identity $Username -Add @{extensionAttribute1= Get-Date -Format "MMddyyyyHHmm"}

    #Set new password for inactive users
    Set-ADAccountPassword -Identity $Username -NewPassword $NewPassword -Reset
}

#Get all AD users with SAMAccountname and LastLogonDate
Get-ADUser -Filter * -Properties SAMAccountname,LastLogonDate | ForEach-Object {
    $SAMAccountname = $_.SAMAccountname
    
    #Check if users last logon date is older than today minus specified days
    if($_.lastlogondate -lt (Get-date).AddDays(-$deactivateafterdays)){
        #Call revoke function with samaccountname
        Revoke-User -Username "$SAMAccountname" -NewPassword (Get-RandomPassword 18 4 4 4)

        #Log who was revoked to terminal
        Write-Host "$SAMAccountname - $($_.lastlogondate) - Revoked"
    }
}