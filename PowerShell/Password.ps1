﻿function Get-RandomPassword {
    param (
        [Parameter(Mandatory)]
        [int] $length,
        [int] $uppercount = 1,
        [int] $lowercount = 1,
        [int] $specialcount = 1)

    #Check if the parametars is correct else throw an error about it
    if($uppercount + $lowercount + $specialcount -gt $length) {
        throw "Error: Length needs to be greater than the sum of criteria"
    }
    else{
        $numbercount = $length - $uppercount - $lowercount - $specialcount
    }

    #Define the character set that can be in the password
    $uCharSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    $lCharSet = "abcdefghijklmnopqrstuvwxyz"
    $nCharSet = "0123456789"
    $sCharSet = " !`"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
    
    #Define the arraylist so I can use the Add method
    $unsecpassword = [System.Collections.ArrayList]@()
    
    #Fill the array with randoms for the given amount
    for ($i = 0; $i -lt $uppercount; $i++){
        $null = $unsecpassword.Add($uCharSet[(Get-Random -Maximum $uCharSet.Length)])
    }
    for ($i = 0; $i -lt $lowercount; $i++){
        $null = $unsecpassword.Add($lCharSet[(Get-Random -Maximum $lCharSet.Length)])
    }
    for ($i = 0; $i -lt $specialcount; $i++){
        $null = $unsecpassword.Add($sCharSet[(Get-Random -Maximum $sCharSet.Length)])
    }
    for ($i = 0; $i -lt $numbercount; $i++){
        $null = $unsecpassword.Add($nCharSet[(Get-Random -Maximum $nCharSet.Length)])
    }

    #Shuffle the array and join them to one string. After that convert to secure string
    $secpassword = ConvertTo-SecureString -String (($unsecpassword | Sort-Object {Get-Random}) -join "") -AsPlainText -Force

    return $secpassword
}

#To test the function returns:
#ConvertFrom-SecureString (Get-RandomPassword 12 3 3 3) -AsPlainText
#Get-RandomPassword 12 3 3 3